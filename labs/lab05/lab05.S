#include "hardware/regs/addressmap.h"
#include "hardware/regs/m0plus.h"
.syntax unified                   @ Specify unified assembly syntax
.cpu cortex-m0plus                @ Specify CPU type is Cortex M0+
.thumb                            @ Specify thumb assembly for RP2040
.global main_asm                  @ Provide program starting address to the linker
.align 4                          @ Specify code alignment
.equ SLEEP_TIME, 500              @ Specify the sleep time (in ms)
.equ LED_GPIO_PIN, 25             @ Specify the pin that the LED is connected to
.equ LED_GPIO_OUT, 1              @ Specify the direction of the GPIO pin
.equ LED_VALUE_ON, 1              @ Specify the value that turns the LED "on"
.equ LED_VALUE_OFF, 0             @ Specify the value that turns the LED "off"
.equ SVC_ISR_OFFSET, 0x2C         @ The SVC is entry 11 in the vector table
.equ SVC_MAX_INSTRS, 0x01         @ Maximum allowed SVC subroutines
@ Entry point to the ASM portion of the program
main_asm:
 bl init_gpio_led                 @ Initialise the GPIO LED pin
 bl install_svc_isr               @ Install the SVC interrupt service routine 
loop:
 svc #0                           @ Call the SVC ISR with value 0 (turns on LED)
 nop                              @ Add a no-op instruction for alignment after SVC
 bl do_sleep                      @ Short pause before proceeding
 svc #1                           @ Call the SVC ISR with value 1 (turns off LED)
 nop                              @ Add a no-op instruction for alignment after SVC
 bl do_sleep                      @ Add a short pause before proceeding
 b loop                           @ Always jump back to the start of the loop


@ Subroutine used to introduce a short delay in the application
@ Parameter:
@ R0 -sleepTime : Sleep time in milliseconds
do_sleep:
    push {lr}
    ldr     r0, =SLEEP_TIME             @ Set the value of SLEEP_TIME we want to wait for
    bl      sleep_ms                    @ Sleep until SLEEP_TIME has elapsed then toggle the LED GPIO pin
    pop {pc}

@ Subroutine used to initialise the PI Pico built-in LED
init_gpio_led:
    push {lr}
    movs    r0, #LED_GPIO_PIN           @ This value is the GPIO LED pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #LED_GPIO_PIN           @ This value is the GPIO LED pin on the PI PICO board
    movs    r1, #LED_GPIO_OUT           @ We want this GPIO pin to be setup as an output pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1    
    pop {pc}                      
@ Subroutine used to install the SVC interrupt service handler
install_svc_isr:
 ldr r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)          @ Accesss the hardward register which contains the address of IVT
 ldr r1, [r2]                                      @ Load the address of IVT to R1
 movs r2, #SVC_ISR_OFFSET                          @ Load offset to SVC slot in the IVT which is 4 * 11 = 0x2c
 add r2, r1                                        @ the intAdd the address of IVT with the offset to SVC entry (entry 11)
 ldr r0, =svc_isr                                  @ Load the address of errupt handler svc_isr to R0
 str r0, [r2]                                      @ Store the address of svc_isr to the SVC entry in the IVT
 bx lr                                             @ Exit the function and goes back to the calling place
@ SVC interrupt service handler routine              
.thumb_func                                        @ Required for all interrupt service routines
svc_isr:                                               
 push {lr}                                         @ Push the return address to the stack
 ldr r0, [sp, #0x1C]                               @ To retrieve the address of the SVC opcode by going back 0x1c from the top of the stack
 subs r0, #0x2                                     @ Minus two from the address
 ldr r0, [r0]                                      @ Load the opcode from the address
 ldr r1, =#0xFF                                    @ Load 0xff to R1
 ands r0, r1                                       @ Extract the data stored in the 8-bit comment field in the opcode
 cmp r0, #SVC_MAX_INSTRS                           @ Compare the comment stored with the number: 1
 bgt svc_done                                      @ If it is greater than 1 then branch to svc_done and exit the handler
 adr r1, svc_jmptbl                                @ Else load the address of SVC handler table to R1
 lsls r0, #2                                       @ Logical shift left by two bits which is equavelent of multiply by 4 since the data in svc_jmptbl is align by 4
 ldr r1, [r1, r0]                                  @ Add the entry point address of the handler into R1
 mov pc, r1                                        @ Load the handler address to the PC and the program will jump to the corresponding handler
svc_done:
 pop {pc}                                          @ Pop the return address from the stack and store to PC and exit

@ First function of SVC subroutine - turn on the LED
svc_num0:
    movs r0, #LED_GPIO_PIN                         @ Load the LED pin number to R0 which is pin 25
    movs r1, #LED_VALUE_ON                         @ The LED is currently "on" so we want to turn it "off" 
    bl      asm_gpio_put                           @ Update the the value of the LED GPIO pin (based on value in r1)
    b svc_done                                     @ Branch back to the main ISR when done

@ Second function of SVC subroutine - turn off the LED
svc_num1:
@ <TODO – add assembly code to turn off the LED>
    movs r0, #LED_GPIO_PIN                         @ Load the LED pin number to R0 which is pin 25        
    movs r1, #LED_VALUE_OFF                        @ Load the value "OFF" to R1
    bl      asm_gpio_put                           @ Update the the value of the LED GPIO pin (based on value in r1)
    b svc_done                                     @ Branch back to the main ISR when done
@ SVC function entry jump table.           
.align 2
svc_jmptbl:
 .word svc_num0                                    @ Entry zero goes to SVC function #0.
 .word svc_num1                                    @ Entry one goes to SVC function #1.
 .word 0                                           @ Null termination of the jump table.
@ Set data alignment
.data
 .align 4
