#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "pico/stdlib.h"
#include "pico/float.h" // Required for using single-precision variables.
#include "pico/double.h" // Required for using double-precision variables.
#include "hardware/timer.h"


// #define XIP_CTRL_BASE 0x14000000
#define XIP_FLUSH_OFFSET 0x04
/**
 * @brief A function that takes snapshot of the timestamps and return the (value/10000)
 * 
 * @return clock_t The timestamps in 10^-4 form
 */
clock_t takeSnapshot(){
    return (clock_t) time_us_64() / 10000;
}


/**
 * @brief A function that calculates Pi using Wallis product in single precision and tends to N
 * @param N The upper bound of the Wallis product
 */
float wallis_product_float(int N){
    // initialize the final product
    float product = 1;
    for(int n = 1; n <= N; n++){
        // multiply product with the expression on the right and update product with the new value
       product *= ( (2.0*n) / ((2*n) - 1) ) * ( (2.0*n) / ((2*n) + 1) );
    }   
    // return Pi since Pi = 2 * product;
    return product*2;

}
/**
 * @brief A wrapper function of wallis_product_float(), this is needed because I cannot 
 * turn off the compiler optimization even though I choose "Debug" as the build type in VS Code
 * as well as adding set(PICO_DEOPTIMIZED_DEBUG 1) and set(CMAKE_BUILD_TYPE Debug) to
 * CMakeLists.txt. This does not affect the running time of the wallis_product_float().
 * 
 * @param N 
 * @return float 
 */
float wrapper_wallis_product_float1(int N){
    return wallis_product_float(N);
}
/**
 * @brief A wrapper function of wallis_product_float(), this is needed because I cannot 
 * turn off the compiler optimization even though I choose "Debug" as the build type in VS Code
 * as well as adding set(PICO_DEOPTIMIZED_DEBUG 1) and set(CMAKE_BUILD_TYPE Debug) to
 * CMakeLists.txt. This does not affect the running time of the wallis_product_float().
 * 
 * @param N 
 * @return float 
 */
float wrapper_wallis_product_float2(int N){
    return wallis_product_float(N);
}
/**
 * @brief A wrapper function of wallis_product_float(), this is needed because I cannot 
 * turn off the compiler optimization even though I choose "Debug" as the build type in VS Code
 * as well as adding set(PICO_DEOPTIMIZED_DEBUG 1) and set(CMAKE_BUILD_TYPE Debug) to
 * CMakeLists.txt. This does not affect the running time of the wallis_product_float().
 * 
 * @param N 
 * @return float 
 */
float wrapper_wallis_product_float3(int N){
    return wallis_product_float(N);
}

/**
 * @brief A function that calculates Pi using Wallis product in double precision and tends to N
 * @param N The upper bound of the Wallis product
 */
double wallis_product_double(int N){
    // initialize the final product
    double product = 1;
    for(int n = 1; n <= N; n++){
        // multiply product with the expression on the right and update product with the new value
        product *= ( (2.0*n) / ((2*n) - 1) ) * ( (2.0*n) / ((2*n) + 1) );
    }   
    // return Pi since Pi = 2 * product;
    return product*2;
}
/**
 * @brief A wrapper function of wallis_product_double(), this is needed because I cannot 
 * turn off the compiler optimization even though I choose "Debug" as the build type in VS Code
 * as well as adding set(PICO_DEOPTIMIZED_DEBUG 1) and set(CMAKE_BUILD_TYPE Debug) to
 * CMakeLists.txt. This does not affect the running time of the wallis_product_double().
 * 
 * @param N 
 * @return double 
 */
double wrapper_wallis_product_double1(int N){
    return wallis_product_double(N);
}
/**
 * @brief A wrapper function of wallis_product_double(), this is needed because I cannot 
 * turn off the compiler optimization even though I choose "Debug" as the build type in VS Code
 * as well as adding set(PICO_DEOPTIMIZED_DEBUG 1) and set(CMAKE_BUILD_TYPE Debug) to
 * CMakeLists.txt. This does not affect the running time of the wallis_product_double().
 * 
 * @param N 
 * @return double 
 */
double wrapper_wallis_product_double2(int N){
    return wallis_product_double(N);
}

/**
* @brief This function acts as the main entry-point for core #1.
* A function pointer is passed in via the FIFO with one
* incoming int32_t used as a parameter. The function will
* provide an int32_t return value by pushing it back on
* the FIFO, which also indicates that the result is ready.
*/
void core1_entry() {
 while (1) {
    // get the pointer to a function that return a double
 double (*func)() = (double(*)()) multicore_fifo_pop_blocking();
 // get ITER_MAX passed in from core0
 int p = multicore_fifo_pop_blocking();
 // call the subroutine
 double result = (*func)(p);
//  printf("Double precision result from core 1 before push to fifo: %.20f\n", result);
// return the result of the subroutine
 multicore_fifo_push_blocking(result);
 }
}

// Function to get the enable status of the XIP cache
bool get_xip_cache_en() {
    int * pointer = (int *) XIP_CTRL_BASE; // create a pointer to XIP_CTRL_BASE
    int firstByte = pointer[0]; // get the content of the first byte
    int status = firstByte & 1; // read bit 0
    printf("XIP Enable status: %d\n", status);
    return status == 1;
}
// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en) {
    int volatile * const pointer = (int * ) XIP_CTRL_BASE;
    if(cache_en){
        *pointer = 1; // writing 1 to bit 0 to enable cache
    }
    else if(!cache_en){
        *pointer = 0x10; // writing 0 to bit 0 to disable cache
    }
    return true;
}



// Main code entry point for core0.
int main() {
    stdio_init_all();
    const int ITER_MAX = 100000;
    // testing the correct functioning of the two methods. We can also conclude that XIP cache is enabled by default on CP2040
    printf("Testing get_xip_cache_en() and set_xip_cache_en()\n");
    get_xip_cache_en();
    set_xip_cache_en(false);
    get_xip_cache_en();
    set_xip_cache_en(true);
    get_xip_cache_en();
    ///////////////////////////////////////////////////////  Cache enabled ///////////////////////////////////////////////
    // Running the code sequentially
    // Single precision
    printf("Cache Enabled...\n");

    clock_t startTimeSinglePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 
    float single_precision = wallis_product_float(ITER_MAX); // calculating wallis product in single precision
    clock_t endTimeSinglePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer

    // Double precision
    clock_t startTimeDoublePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 
    double double_precision = wallis_product_double(ITER_MAX); // calculating wallis product in single precision
    clock_t endTimeDoublePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 


    // Calculate the execution time of single precision calculation in single core mode
    double executionTimeSinglePrecisionSingleCore = (double)(endTimeSinglePrecisionSingleCore - startTimeSinglePrecisionSingleCore)/100;
    printf("Single Precision Single core run time: %.8f sec\n", executionTimeSinglePrecisionSingleCore); // printing the result

    // Calculate the execution time of double precision calculation in single core mode
    double executionTimeDoublePrecisionSingleCore = (double)(endTimeDoublePrecisionSingleCore - startTimeDoublePrecisionSingleCore)/100;
    printf("Double Precision Single core run time: %.8f sec\n", executionTimeDoublePrecisionSingleCore);

    // Calculate the oatl execution time in single core mode
    double totalTimeSequential = executionTimeDoublePrecisionSingleCore + executionTimeSinglePrecisionSingleCore;
    printf("Single core total run time: %.8f sec\n", totalTimeSequential ); // printing the result
    
    printf("Sequential: Single Precision: %.20f\n",single_precision);
    printf("Sequential: Double Precision: %.20f\n",double_precision);
    
    
    // Using dual core
    clock_t statrTimeMultiCore = takeSnapshot(); // take a snapshot of the timer at the start of the multicore execuction
    
    clock_t startTimeDoublePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    multicore_launch_core1(core1_entry); // entry point to core 1
    // Running double precision on core 1
    multicore_fifo_push_blocking((uintptr_t) &wallis_product_double); // pass the function pointer to core 1
    multicore_fifo_push_blocking(ITER_MAX); // pass ITER_MAX to core 1


    // Running single precision on core 0
    clock_t startTimeSinglePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    float singlePrecisionMulti = wrapper_wallis_product_float1(ITER_MAX); // have to use a wrapper function because I cannot turn off compiler optimization
    clock_t endTimeSinglePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    double doublePrecisionMulti = multicore_fifo_pop_blocking(); // pop the result of wallis_product_double off from fifo
    clock_t endTimeDoublePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    clock_t endTimeMulticore = takeSnapshot();


    // Calculate the execution time of single precision calculation in multi core mode
    double executionTimeSinglePrecisionMultiCore = (double)(endTimeSinglePrecisionMultiCore - startTimeSinglePrecisionMultiCore)/100;
    printf("Single Precision Multicore run time: %.8f sec\n", executionTimeSinglePrecisionMultiCore);
    // double doublePrecisionMulti = multicore_fifo_pop_blocking(); // pop the result of wallis_product_double off from fifo


    // Calculate the execution time of double precision calculation in multi core mode
    double executionTimeDoublePrecisionMultiCore = (double)(endTimeDoublePrecisionMultiCore - startTimeDoublePrecisionMultiCore)/100;
    printf("Double Precision Multi core run time: %.8f sec\n", executionTimeDoublePrecisionMultiCore); // printing the result

    // Calculate the total execution time using the dual cores
    // double totalTimeMulticore = executionTimeSinglePrecisionMultiCore + executionTimeDoublePrecisionMultiCore;
    double totalTimeMulticore = (double) (endTimeMulticore - statrTimeMultiCore)/100;
    printf("Multi core total run time: %.8f sec\n", totalTimeMulticore );
 
    printf("Multicore: Single Precision: %.20f\n", singlePrecisionMulti);
    printf("Multicore: Double Precision (Note the value inside core1_entry() is correct, but was truncated to 3. Please uncommet the print statement in core1_entry() to verify): %.20f\n", doublePrecisionMulti);


    //////////////////////////////////////////////////////////// Cache disabled ///////////////////////////////////////////////////////////
    printf("Cache Disabled...\n");
    set_xip_cache_en(false); // disable XIP cache
    multicore_reset_core1(); // reset core1


    startTimeSinglePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 
    single_precision = wrapper_wallis_product_float3(100000); // calculating wallis product in single precision
    endTimeSinglePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer

    // Double precision
    startTimeDoublePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 
    double_precision = wrapper_wallis_product_double1(100000); // calculating wallis product in single precision
    endTimeDoublePrecisionSingleCore = takeSnapshot(); // take a snapshot of the timer 


    // Calculate the execution time of single precision calculation in single core mode
    executionTimeSinglePrecisionSingleCore = (double)(endTimeSinglePrecisionSingleCore - startTimeSinglePrecisionSingleCore)/100;
    printf("Single Precision Single core run time: %.8f sec\n", executionTimeSinglePrecisionSingleCore); // printing the result

    // Calculate the execution time of double precision calculation in single core mode
    executionTimeDoublePrecisionSingleCore = (double)(endTimeDoublePrecisionSingleCore - startTimeDoublePrecisionSingleCore)/100;
    printf("Double Precision Single core run time: %.8f sec\n", executionTimeDoublePrecisionSingleCore);

    // Calculate the oatl execution time in single core mode
    totalTimeSequential = executionTimeDoublePrecisionSingleCore + executionTimeSinglePrecisionSingleCore;
    printf("Single core total run time: %.8f sec\n", totalTimeSequential ); // printing the result
    
    printf("Sequential: Single Precision: %.20f\n",single_precision);
    printf("Sequential: Double Precision: %.20f\n",double_precision);
    
    
    // Using dual core
    statrTimeMultiCore = takeSnapshot(); // take a snapshot of the timer at the start of the multicore execuction
    
    startTimeDoublePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    multicore_launch_core1(core1_entry); // entry point to core 1
    // Running double precision on core 1
    multicore_fifo_push_blocking((uintptr_t) &wallis_product_double); // pass the function pointer to core 1
    multicore_fifo_push_blocking(ITER_MAX); // pass ITER_MAX to core 1


    // Running single precision on core 0
    startTimeSinglePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    singlePrecisionMulti = wrapper_wallis_product_float2(100000); // have to use a wrapper function because I cannot turn off compiler optimization
    endTimeSinglePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    doublePrecisionMulti = multicore_fifo_pop_blocking(); // pop the result of wallis_product_double off from fifo
    endTimeDoublePrecisionMultiCore = takeSnapshot(); // take a snapshot of the timer 
    endTimeMulticore = takeSnapshot();


    // Calculate the execution time of single precision calculation in multi core mode
    executionTimeSinglePrecisionMultiCore = (double)(endTimeSinglePrecisionMultiCore - startTimeSinglePrecisionMultiCore)/100;
    printf("Single Precision Multicore run time: %.8f sec\n", executionTimeSinglePrecisionMultiCore);


    // Calculate the execution time of double precision calculation in multi core mode
    executionTimeDoublePrecisionMultiCore = (double)(endTimeDoublePrecisionMultiCore - startTimeDoublePrecisionMultiCore)/100;
    printf("Double Precision Multi core run time: %.8f sec\n", executionTimeDoublePrecisionMultiCore); // printing the result

    // Calculate the total execution time using the dual cores
    totalTimeMulticore = (double) (endTimeMulticore - statrTimeMultiCore)/100;
    printf("Multi core total run time: %.8f sec\n", totalTimeMulticore );
 
    printf("Multicore: Single Precision: %.20f\n", singlePrecisionMulti);
    printf("Multicore: Double Precision (Note the value inside core1_entry() is correct, but was truncated to 3. Please uncommet the print statement in core1_entry() to verify): %.20f\n", doublePrecisionMulti);

    return 0;
}
