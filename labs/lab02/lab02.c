#define WOKWI
#define PI 3.14159265359
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "pico/double.h" // Required for using double-precision variables.
#include "pico/float.h"  // Required for using single-precision variables.


//pico/stdlib.h cause error in the Wokwi simulator so it was not included
#ifndef WOKWI
#include "pico/stdlib.h"
#endif

/**
 * @brief A function that calculates Pi using Wallis product in single precision and tends to N
 * @param N The upper bound of the Wallis product
 */
 
float wallis_product_float(int N){
    // initialize the final product
    float product = 1;
    for(int n = 1; n <= N; n++){
        // multiply product with the expression on the right and update product with the new value
       product *= ( (2.0*n) / ((2*n) - 1) ) * ( (2.0*n) / ((2*n) + 1) );
    }   
    // return Pi since Pi = 2 * product;
    return product*2;

}
/**
 * @brief A function that calculates Pi using Wallis product in double precision and tends to N
 * @param N The upper bound of the Wallis product
 */
double wallis_product_double(int N){
    // initialize the final product
    double product = 1;
    for(int n = 1; n <= N; n++){
        // multiply product with the expression on the right and update product with the new value
        product *= ( (2.0*n) / ((2*n) - 1) ) * ( (2.0*n) / ((2*n) + 1) );
    }   
    // return Pi since Pi = 2 * product;
    return product*2;
}


/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif
    //Calculating for single precision (float)
    float single_precision = wallis_product_float(100000);

    // Calculating approxiamation error
    float single_precision_error = (PI - single_precision);

    //Calculating for double precision (double)
    double double_precision = wallis_product_double(100000);

    // Calculating approxiamation error
    double double_precision_error = (PI - double_precision);
    
    // Print a console message to inform user what's going on.
    printf("Single Precision: %.20f\n",single_precision);
    printf("Approximation Error of Single Pecision: %.20f\n",single_precision_error);

    printf("Double Precision: %.20f\n",double_precision);
    printf("Approximation Error of Double Pecision: %.20f\n",double_precision_error);

    // Returning zero indicates everything went okay.
    return 0;
}

