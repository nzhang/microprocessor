#include "pico/stdlib.h"


/**
 * @brief Take LED pin number and sleep delay in ms and 
 *         toggles the LED specified by the pin number
 *          on and off forever
 * 
 * @param LEDPinNumber - PIN number of the LED
 * @param sleepDelay - delay in ms
 * 
 * @return  void
*/
void blink_LED(uint LEDPinNumber, uint sleepDelay){
    // Do forever...
    while (true) {
        // Toggle the LED on and then sleep for delay period
        gpio_put(LEDPinNumber, 1);
        sleep_ms(sleepDelay);
        // Toggle the LED off and then sleep for delay period
        gpio_put(LEDPinNumber, 0);
        sleep_ms(sleepDelay);
    }
}

/**
 * @brief LAB #01 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    //calling blink_LED()
    blink_LED(LED_PIN, LED_DELAY);

    // Should never get here due to infinite while-loop.
    return 0;

}
